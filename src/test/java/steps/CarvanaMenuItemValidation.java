package steps;

import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.NotFoundException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import pages.*;
import utilities.Driver;
import utilities.Waiter;

public class CarvanaMenuItemValidation {

    public WebDriver driver;
    public CarvanaHomePage carvanaHomePage;
    public CarFinderPage carFinderPage;
    public CarValuePage carValuePage;
    public CarSellPage carSellPage;
    public CarvanaAutoLoanCalculator carvanaAutoLoanCalculator;

    @Before
    public void setUp() {
        driver = Driver.getDriver();
        carvanaHomePage = new CarvanaHomePage(driver);
        carFinderPage = new CarFinderPage(driver);
        carValuePage = new CarValuePage(driver);
        carSellPage = new CarSellPage(driver);
        carvanaAutoLoanCalculator = new CarvanaAutoLoanCalculator(driver);
    }

    @Given("user is on {string}")
    public void userIsOn(String URL) {
        driver.get(URL);
        Assert.assertEquals(URL, driver.getCurrentUrl());
    }

    @When("user clicks on {string} menu item")
    public void userClicksOnMenuItem(String elementText) {
        if (elementText.equals(carvanaHomePage.carFinderLink.getText())){
            carvanaHomePage.carFinderLink.click();
        }else if (elementText.equals(carvanaHomePage.carSellTradeLink.getText())){
            carvanaHomePage.carSellTradeLink.click();
        }
    }

    @Then("user should be navigated to {string}")
    public void userShouldBeNavigatedTo(String carFinderURL) {
        Assert.assertEquals(driver.getCurrentUrl(), carFinderURL);
    }

    @And("user should see {string} text")
    public void userShouldSeeText(String whatCar) {
        WebElement element;
        switch (whatCar) {
            case "WHAT CAR SHOULD I GET?":
                element = carFinderPage.firstHeaderInSearchPage;
                break;
            case "Car Finder can help! Answer a few quick questions to find the right car for you":
                element = carFinderPage.thirdHeaderInSearchPage;
                break;
            case "What is most important to you in your next car?":
                element = carValuePage.importanceInCar;
                break;
            case "Select up to 4 in order on importance":
                element = carValuePage.fourImportantInCar;
                break;
            case "GET A REAL OFFER IN 2 MINUTES":
                element = carSellPage.getOfferInTwoMinutes;
                break;
            case "We pick up your car. You get paid on the spot.":
                element = carSellPage.pickUpCarText;
                break;
            case "We couldn’t find that VIN. Please check your entry and try again.":
                element = carSellPage.errorMessage;
                driver.navigate().refresh();
                Waiter.waitForWebElementToBeVisible(driver, 60, element);
                break;
            default:
                throw new NotFoundException(whatCar + " is not found");
        }
        Assert.assertTrue(element.isDisplayed());
    }

    @And("user should see {string} link")
    public void userShouldSeeLink(String tryCar) {
        Assert.assertTrue(carFinderPage.tryCarFinderLink.isDisplayed());
    }

    @When("user clicks on {string} link")
    public void userClicksOnLink(String arg0) {
        carFinderPage.tryCarFinderLink.click();
    }

    @When("user clicks on {string} button")
    public void userClicksOnButton(String text) {
            carSellPage.vinButton.click();
    }


    @And("user enters vin number as {string}")
    public void userEntersVinNumberAs(String vinNumber) {
        carSellPage.inputBox.sendKeys(vinNumber);
        carSellPage.getOfferButton.click();
    }

    @When("user hovers over on {string} menu item")
    public void userHoversOverOnMenuItem(String arg0) {
        Actions actions = new Actions(driver);
        Waiter.pause(2);
        actions.moveToElement(carvanaHomePage.financingLink).perform();
        Waiter.pause(2);
        carvanaHomePage.autoLoanCalculator.click();
    }

    @When("user enters {string} as {string}")
    public void userEntersAs(String input, String actual) {
        carvanaAutoLoanCalculator.costOfCar.sendKeys("10000");
        carvanaAutoLoanCalculator.downPayment.sendKeys("1500");
    }

    @And("user selects {string} as {string}")
    public void userSelectsAs(String arg0, String arg1) {
        Select select = new Select(carvanaAutoLoanCalculator.creditScore);
        select.selectByValue("3");
        Select select1 = new Select(carvanaAutoLoanCalculator.loanTerms);
        select1.selectByValue("60");
    }

    @Then("user should see the monthly payment as {string}")
    public void userShouldSeeTheMonthlyPaymentAs(String actual) {
        Assert.assertEquals(actual, carvanaAutoLoanCalculator.price.getText());
    }
}
