package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CarValuePage {

    public CarValuePage(WebDriver driver){
        PageFactory.initElements(driver, this);
    }

    @FindBy(className = "styles__Headline-sc-1z43k2-1")
    public WebElement importanceInCar;

    @FindBy(css = "div[data-qa='sub-heading']")
    public WebElement fourImportantInCar;
}
