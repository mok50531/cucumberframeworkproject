package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CarFinderPage {

    public CarFinderPage(WebDriver driver){
        PageFactory.initElements(driver, this);
    }

    @FindBy(tagName = "h1")
    public WebElement firstHeaderInSearchPage;

    @FindBy(tagName = "h3")
    public WebElement thirdHeaderInSearchPage;

    @FindBy(css = "a[data-qa='router-link']")
    public WebElement tryCarFinderLink;
}
