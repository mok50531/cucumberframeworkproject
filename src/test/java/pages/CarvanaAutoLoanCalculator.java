package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CarvanaAutoLoanCalculator {

    public CarvanaAutoLoanCalculator(WebDriver driver){
        PageFactory.initElements(driver, this);
    }

    @FindBy(name = "vehiclePrice")
    public WebElement costOfCar;

    @FindBy(id = "creditBlock")
    public WebElement creditScore;

    @FindBy(name = "loanTerm")
    public WebElement loanTerms;

    @FindBy(name = "downPayment")
    public WebElement downPayment;

    @FindBy(className = "loan-calculator-display-value")
    public WebElement price;
}
