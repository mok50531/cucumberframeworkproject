package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CarSellPage {

    public CarSellPage(WebDriver driver){
        PageFactory.initElements(driver, this);
    }

    @FindBy(css = "div[class='typographystyles__HeadlineBase-sc-13s4tcn-4 typographystyles__BrandTwo-sc-13s4tcn-6 Title__Text-sc-l5e5dw-0 gZRddl fKlQzD kVwLTY']")
    public WebElement getOfferInTwoMinutes;

    @FindBy(className = "farOJZ")
    public WebElement pickUpCarText;

    @FindBy(className = "dkObNV")
    public WebElement vinButton;

    @FindBy(css = "div[class='ErrorMessageContainer-oo6j68-8 bEWysd']")
    public WebElement errorMessage;

    @FindBy(css = "input[class='FormInput-oo6j68-5 jLejJf']")
    public WebElement inputBox;

    @FindBy(css = "button[data-cv-test='heroGetMyOfferButton']")
    public WebElement getOfferButton;
}
